package cn.bingoogolapple.swipeitemlayout;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Attr;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.ComponentParent;
import ohos.agp.components.DragInfo;
import ohos.agp.components.ListContainer;
import ohos.agp.components.StackLayout;
import ohos.agp.components.VelocityDetector;
import ohos.agp.utils.TextTool;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.multimodalinput.event.TouchEvent;

/**
 * BGASwipeItemLayout
 *
 * @since 2021-04-22
 */
public class BGASwipeItemLayout extends StackLayout implements Component.DraggedListener,
        Component.BindStateChangedListener, ComponentContainer.ArrangeListener, Component.TouchEventListener {
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, "BGASwipeItemLayout");
    private static final float ALPHA_START = 0.1f;
    private static final float ALPHA_MULTI = 0.9f;
    private static final int CHILD_NUM = 2;
    private VelocityDetector mVelocityTracker;

    // 顶部视图
    private Component mTopView;

    // 底部视图
    private Component mBottomView;

    // 拖动的弹簧距离
    private int mSpringDistance = 0;

    // 允许拖动的距离【注意：最终允许拖动的距离是 (mDragRange + mSpringDistance)】
    private int mDragRange;

    // 控件滑动方向（向左，向右），默认向左滑动
    private SwipeDirection mSwipeDirection = SwipeDirection.Left;

    // 移动过程中，底部视图的移动方式（拉出，被顶部视图遮住），默认是被顶部视图遮住
    private BottomModel mBottomModel = BottomModel.PullOut;

    // 滑动控件当前的状态（打开，关闭，正在移动），默认是关闭状态
    private Status mCurrentStatus = Status.Closed;


    // 滑动控件滑动前的状态
    private Status mPreStatus = mCurrentStatus;

    // 顶部视图下一次layout时的left
    private int mTopLeft;

    // 顶部视图外边距
    private ComponentContainer.LayoutConfig mTopLp;

    // 底部视图外边距
    private ComponentContainer.LayoutConfig mBottomLp;

    // 滑动比例，【关闭->展开  =>  0->1】
    private float mDragRatio;

    // 手动拖动打开和关闭代理
    private BGASwipeItemLayoutDelegate mDelegate;

    private LongClickedListener mOnLongClickListener;
    private ClickedListener mOnClickListener;
    /**
     * 是否可滑动
     */
    private boolean mSwipeable = true;
    /**
     * 是否有弹簧
     */
    private boolean mSpringAble = true;

    private Runnable mCancelPressedTask = new Runnable() {
        @Override
        public void run() {
            setPressState(false);
        }
    };

    // 最后的坐标点
    private float finalX;
    private float finalY;

    // 拖动的距离
    private int distance;
    private float alpha = 0;
    private boolean leftOut = false;
    private boolean rightOut = false;
    private ListContainer defaultResult = null;

    /**
     * 构造器
     *
     * @param context 上下文
     * @param attrs 属性集合
     */
    public BGASwipeItemLayout(Context context, AttrSet attrs) {
        this(context, attrs, "");
    }

    /**
     * 构造器
     *
     * @param context 上下文
     * @param attrs 属性集合
     * @param styleName 样式名称
     */
    public BGASwipeItemLayout(Context context, AttrSet attrs, String styleName) {
        super(context, attrs, styleName);
        initAttrs(context, attrs);
        initListener();
    }

    private void initAttrs(Context context, AttrSet attrs) {
        final int attrsLength = attrs.getLength();
        for (int index = 0; index < attrsLength; index++) {
            initAttr(context, index, attrs);
        }
    }

    private void initAttr(Context context, int index, AttrSet attrSet) {
        if (attrSet.getAttr(index).isPresent()) {
            Attr attr = attrSet.getAttr(index).get();
            if (TextTool.isEqual(attr.getName(), context.getString(ResourceTable.String_bga_sil_swipeDirection))) {
                // 默认向左滑动
                String directionValue = attr.getStringValue();
                if (TextTool.isEqual(directionValue, "right")) {
                    mSwipeDirection = SwipeDirection.Right;
                }
            } else if (TextTool.isEqual(attr.getName(), context.getString(ResourceTable.String_bga_sil_bottomMode))) {
                // 默认是拉出
                String bottomMode = attr.getStringValue();
                if (TextTool.isEqual(bottomMode, "layDown")) {
                    mBottomModel = BottomModel.LayDown;
                }
            } else if (TextTool.isEqual(attr.getName(),
                    context.getString(ResourceTable.String_bga_sil_springDistance))) {
                // 弹簧距离，不能小于0，默认值为0
                mSpringDistance = attr.getDimensionValue();
                if (mSpringDistance < 0) {
                    throw new IllegalStateException("bga_sil_springDistance不能小于0");
                }
            } else if (TextTool.isEqual(attr.getName(), context.getString(ResourceTable.String_bga_sil_swipeAble))) {
                mSwipeable = attr.getBoolValue();
            } else if (TextTool.isEqual(attr.getName(), context.getString(ResourceTable.String_bga_sil_springAble))) {
                mSpringAble = attr.getBoolValue();
            }
        }
    }

    private void initListener() {
        setBindStateChangedListener(this);
        setArrangeListener(this);
        setTouchEventListener(this);
        setDraggedListener(Component.DRAG_HORIZONTAL, this);
    }

    public void setDelegate(BGASwipeItemLayoutDelegate delegate) {
        mDelegate = delegate;
    }

    public void setSwipeAble(boolean swipeAble) {
        mSwipeable = swipeAble;
    }

    @Override
    public void onComponentBoundToWindow(Component component) {
        if (getChildCount() < CHILD_NUM) {
            throw new IllegalStateException(BGASwipeItemLayout.class.getSimpleName() + "必须有且只有两个子控件");
        }
        mTopView = getComponentAt(1);
        mBottomView = getComponentAt(0);

        // 避免底部视图被隐藏时还能获取焦点被点击
        mBottomView.setVisibility(INVISIBLE);

        mTopLp = mTopView.getLayoutConfig();
        mBottomLp = mBottomView.getLayoutConfig();
        mTopLeft = /* getPaddingLeft() + */ mTopLp.getMarginLeft();

        // 判断外围是否是ListContainer
        if (insideAdapterView()) {
            if (mOnClickListener == null) {
                setClickedListener(new ClickedListener() {
                    @Override
                    public void onClick(Component v) {
                        performAdapterViewItemClick();
                    }
                });
            }
            if (mOnLongClickListener == null) {
                setLongClickedListener(new LongClickedListener() {
                    @Override
                    public void onLongClicked(Component v) {
                        performAdapterViewItemLongClick();
                    }
                });
            }
        }
    }

    @Override
    public void onComponentUnboundFromWindow(Component component) {
    }

    @Override
    public void onDragDown(Component component, DragInfo dragInfo) {
        HiLog.debug(LABEL, "onDragDown:" + dragInfo.yOffset);
    }

    @Override
    public void onDragStart(Component component, DragInfo dragInfo) {
        HiLog.debug(LABEL, "onDragStart:" + dragInfo.yOffset);
    }

    @Override
    public void onDragUpdate(Component component, DragInfo dragInfo) {
        HiLog.debug(LABEL, "onDragUpdate:" + dragInfo.yOffset);
    }

    @Override
    public void onDragEnd(Component component, DragInfo dragInfo) {
        HiLog.debug(LABEL, "onDragEnd:" + dragInfo.yOffset);
    }

    @Override
    public void onDragCancel(Component component, DragInfo dragInfo) {
        HiLog.debug(LABEL, "onDragCancel:" + dragInfo.yOffset);
    }

    @Override
    public boolean onDragPreAccept(Component component, int dragDirection) {
        HiLog.debug(LABEL, "onDragPreAccept:" + dragDirection);
        return false;
    }

    @Override
    public void setClickedListener(ClickedListener clickedListener) {
        super.setClickedListener(clickedListener);
        mOnClickListener = clickedListener;
    }

    @Override
    public void setLongClickedListener(LongClickedListener longClickedListener) {
        super.setLongClickedListener(longClickedListener);
        mOnLongClickListener = longClickedListener;
    }

    private void performAdapterViewItemClick() {
        ComponentParent componentParent = getComponentParent();
        if (componentParent instanceof ListContainer) {
            ListContainer view = (ListContainer) componentParent;
            int position = view.getIndexForComponent(BGASwipeItemLayout.this);
            if (position != ListContainer.INVALID_INDEX) {
                view.executeItemClick(view.getComponentAt(position
                        - view.getFirstVisibleItemPosition()), position, view.getItemProvider().getItemId(position));
            }
        }
    }

    private boolean performAdapterViewItemLongClick() {
        ComponentParent componentParent = getComponentParent();
        if (componentParent instanceof ListContainer) {
            ListContainer view = (ListContainer) componentParent;
            int position = view.getIndexForComponent(BGASwipeItemLayout.this);
            if (position == ListContainer.INVALID_INDEX) {
                return false;
            }
            boolean handled = executeLongClick();
            return handled;
        }
        return false;
    }

    private boolean insideAdapterView() {
        return getAdapterView() != null;
    }

    /** 获取之前的状态
     *
     * @return Status 之前的状态
     */
    public Status getmPreStatus() {
        return mPreStatus;
    }

    private ListContainer getAdapterView() {
        ComponentParent componentParent = getComponentParent();
        if (componentParent instanceof ListContainer) {
            return (ListContainer) componentParent;
        }
        return defaultResult;
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent event) {
        final int springDistance = 60;
        final int percentNum = 2;
        if (!mSwipeable) {
            return false;
        }
        if (mVelocityTracker == null) {
            mVelocityTracker = VelocityDetector.obtainInstance();
        }
        mVelocityTracker.addEvent(event);

        int action = event.getAction();
        switch (action) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                pointDown(event);
                return true;
            case TouchEvent.POINT_MOVE:
                dispatchSwipeEvent();
                pointMove(event,springDistance);
                break;
            case TouchEvent.PRIMARY_POINT_UP:
                pointUp(springDistance,percentNum);
                break;
            case TouchEvent.CANCEL:
                HiLog.debug(LABEL, "ACTION_CANCEL");

                // 此处写一个收缩动画
                break;
            default:
                break;
        }
        return false;
    }

    private void pointDown(TouchEvent event) {
        distance = 0;
        alpha = 0;
        leftOut = false;
        rightOut = false;
        finalX = event.getPointerScreenPosition(0).getX();
        finalY = event.getPointerScreenPosition(0).getY();
    }

    private void pointMove(TouchEvent event,int springDistance) {
        float pointerX = event.getPointerScreenPosition(0).getX();
        float pointerY = event.getPointerScreenPosition(0).getY();
        int offset;
        if (mSwipeDirection == SwipeDirection.Left) {
            // 判断当前滑动最大距离是否达到了最大，如果达到了最大则不允许继续向左滑动
            offset = (int) (finalX - pointerX);
            if (offset > 0) { // 左滑
                // 如果滑块的getleft小于等于滑块的mariginleft-(允许拖动距离+弹簧距离)
                if ((mTopView.getLeft() <= mTopLp.getMarginLeft() - (mDragRange + mSpringDistance))
                        && (mTopLeft >= mTopLp.getMarginLeft() - mDragRange && mSpringAble)) {
                    mTopLeft -= springDistance;
                    postLayout();
                    leftOut = true;
                }
                if (mTopView.getLeft() > mTopLp.getMarginLeft() - (mDragRange + mSpringDistance)) {
                    leftPositiveOutRange(offset,springDistance);
                }
            } else {
                if (mTopView.getLeft() >= /* getPaddingLeft() +*/ mTopLp.getMarginLeft()) {
                    return;
                } else {
                    leftNegativeOutRange(offset);
                }
            }
        } else {
            offset = (int) (pointerX - finalX);
            if (offset > 0) {
                // 判断当前滑动最大距离是否达到了最大，如果达到了最大则不允许继续向右滑动
                if ((mTopView.getLeft() >= mTopLp.getMarginLeft() + (mDragRange + mSpringDistance))
                        && (mTopLeft < mDragRange + springDistance && mSpringAble)) {
                    mTopLeft += springDistance;
                    postLayout();
                    rightOut = true;
                }
                if (mTopView.getLeft() < mTopLp.getMarginLeft()
                        + (mDragRange + mSpringDistance)) {
                    rightPositiveOutRange(offset,springDistance);
                }

            } else {
                if (mTopView.getLeft() <= /* getPaddingLeft() +*/ mTopLp.getMarginLeft()) {
                    return;
                }
                rightNegativeOutRange(offset);
            }
        }
        postLayout();
        finalX = pointerX;
        finalY = pointerY;
    }

    private void pointUp(int springDistance,int percentNum) {
        HiLog.debug(LABEL, "ACTION_UP");
        if (leftOut && mSpringAble) {
            mTopLeft += springDistance;
            mBottomView.setAlpha(1.0f);
            postLayout();
        }
        if (rightOut && mSpringAble) {
            mTopLeft -= springDistance;
            mBottomView.setAlpha(1.0f);
            postLayout();
        }
        if (distance > 0) { // 正向滑动
            if (distance >= mBottomView.getWidth() / percentNum || mPreStatus != Status.Closed) {
                open();
                mBottomView.setAlpha(1.0f);
            } else {
                close();
            }
        } else if (distance < 0) { // 反向滑动
            if (Math.abs(distance) >= mBottomView.getWidth() / percentNum || mPreStatus != Status.Opened) {
                close();
            } else {
                open();
            }
        }
    }

    private void leftPositiveOutRange(int offset,int springDistance) {
        if (distance + offset < (mDragRange + mSpringDistance)) {
            distance += offset;
            mTopLeft = mTopLeft - offset;
            if (mTopLeft < mTopLp.getMarginLeft() - mDragRange) {
                mTopLeft = mTopLp.getMarginLeft() - mDragRange;
            }
            int topViewHorizontalOffset = Math.abs(mTopLeft - mTopLp.getMarginLeft());
            if (topViewHorizontalOffset > mDragRange) {
                mDragRatio = 1.0f;
            } else {
                mDragRatio = 1.0f * topViewHorizontalOffset / mDragRange;
            }

            // 处理底部视图的透明度
            alpha = ALPHA_START + ALPHA_MULTI * mDragRatio;
            mBottomView.setAlpha(alpha);
            if (mBottomView.getVisibility() == Component.INVISIBLE) {
                mBottomView.setVisibility(VISIBLE);
            }

        } else { // 滑动距离过大，直接设置一个默认值
            if (mSpringAble) {
                leftOut = true;
                mTopLeft = mTopLp.getMarginLeft() - mDragRange - springDistance;
            } else {
                mTopLeft = mTopLp.getMarginLeft() - mDragRange;
            }
            mBottomView.setAlpha(1.0f);
        }
    }

    private void leftNegativeOutRange(int offset) {
        if (distance + offset < (mDragRange + mSpringDistance)) {
            distance += offset;
            int topViewHorizontalOffset = Math.abs(mTopLeft - mTopLp.getMarginLeft());
            if (topViewHorizontalOffset > mDragRange) {
                mDragRatio = 1.0f;
            } else {
                mDragRatio = 1.0f * topViewHorizontalOffset / mDragRange;
            }

            // 处理底部视图的透明度
            alpha = ALPHA_START + ALPHA_MULTI * mDragRatio;
            mBottomView.setAlpha(alpha);
            if (mBottomView.getVisibility() == Component.INVISIBLE) {
                mBottomView.setVisibility(VISIBLE);

            }
            mTopLeft = mTopLeft - offset;
            if (mTopLeft >= mTopLp.getMarginLeft()) {
                mTopLeft = mTopLp.getMarginLeft();
            }
        } else {
            mTopLeft = mTopLp.getMarginLeft();
        }
    }

    private void rightPositiveOutRange(int offset,int springDistance) {
        if (distance + offset < (mDragRange + mSpringDistance)) {
            distance += offset;
            int topViewHorizontalOffset = Math.abs(mTopLeft - mTopLp.getMarginLeft());
            if (topViewHorizontalOffset > mDragRange) {
                mDragRatio = 1.0f;
            } else {
                mDragRatio = 1.0f * topViewHorizontalOffset / mDragRange;
            }

            // 处理底部视图的透明度
            alpha = ALPHA_START + ALPHA_MULTI * mDragRatio;
            mBottomView.setAlpha(alpha);
            if (mBottomView.getVisibility() == Component.INVISIBLE) {
                mBottomView.setVisibility(VISIBLE);
            }
            mTopLeft = mTopLeft + offset;
            if (mTopLeft >= mDragRange) {
                mTopLeft = mDragRange;
            }
        } else {
            if (mSpringAble) {
                rightOut = true;
                mTopLeft = mDragRange + springDistance;
            } else {
                mTopLeft = mDragRange;
            }
            mBottomView.setAlpha(1.0f);
        }
    }

    private void rightNegativeOutRange(int offset) {
        if (distance + offset < (mDragRange + mSpringDistance)) {
            distance += offset;
            int topViewHorizontalOffset = Math.abs(mTopLeft - mTopLp.getMarginLeft());
            if (topViewHorizontalOffset > mDragRange) {
                mDragRatio = 1.0f;
            } else {
                mDragRatio = 1.0f * topViewHorizontalOffset / mDragRange;
            }

            // 处理底部视图的透明度
            alpha = ALPHA_START + ALPHA_MULTI * mDragRatio;
            mBottomView.setAlpha(alpha);
            if (mBottomView.getVisibility() == Component.INVISIBLE) {
                mBottomView.setVisibility(VISIBLE);
            }
            mTopLeft = mTopLeft + offset;
            if (mTopLeft <= mTopLp.getMarginLeft()) {
                mTopLeft = mTopLp.getMarginLeft();
            }
        } else {
            mTopLeft = mTopLp.getMarginLeft();
        }
    }

    @Override
    public boolean onArrange(int left, int top, int width, int height) {
        final int heightDecrease = 30;
        mDragRange = mBottomView.getEstimatedWidth() + mBottomLp.getMarginLeft() + mBottomLp.getMarginRight();

        int topTop = /* getPaddingTop() + */mTopLp.getMarginTop();

        int bottomTop = /* getPaddingTop() + */mBottomLp.getMarginTop();

        int bottomLeft;
        int bottomRight;

        if (mSwipeDirection == SwipeDirection.Left) {
            // 向左滑动
            if (mBottomModel == BottomModel.LayDown) {
                // 遮罩，位置固定不变（先计算right，然后根据right计算left）
                bottomRight = getEstimatedWidth() /*- getPaddingRight()*/ - mBottomLp.getMarginRight();
                bottomLeft = bottomRight - mBottomView.getEstimatedWidth();
            } else {
                // 拉出，位置随顶部视图的位置改变
                // 根据顶部视图的left计算底部视图的left
                bottomLeft = mTopLeft + mTopView.getEstimatedWidth() + mTopLp.getMarginRight()
                        + mBottomLp.getMarginLeft();

                // 底部视图的left被允许的最小值
                int minBottomLeft = getEstimatedWidth() /*- getPaddingRight()*/
                        - mBottomView.getEstimatedWidth() - mBottomLp.getMarginRight();

                // 获取最终的left
                bottomLeft = Math.max(bottomLeft, minBottomLeft);

                // 根据left计算right
                bottomRight = bottomLeft + mBottomView.getEstimatedWidth();
            }
        } else {
            // 向右滑动
            if (mBottomModel == BottomModel.LayDown) {

                // 遮罩，位置固定不变（先计算left，然后根据left计算right）
                bottomLeft = /* getPaddingLeft() + */mBottomLp.getMarginLeft();

                bottomRight = bottomLeft + mBottomView.getEstimatedWidth();
            } else {
                // 拉出，位置随顶部视图的位置改变
                // 根据顶部视图的left计算底部视图的left
                bottomLeft = mTopLeft - mDragRange;

                // 底部视图的left被允许的最大值
                int maxBottomLeft = getPaddingLeft() + mBottomLp.getMarginLeft();

                // 获取最终的left
                bottomLeft = Math.min(maxBottomLeft, bottomLeft);

                // 根据left计算right
                bottomRight = bottomLeft + mBottomView.getEstimatedWidth();
            }
        }

        mBottomView.arrange(bottomLeft, bottomTop, mBottomView.getEstimatedWidth(),
                mBottomView.getEstimatedHeight() - heightDecrease);
        mTopView.arrange(mTopLeft, topTop, mTopView.getEstimatedWidth(), mTopView.getEstimatedHeight());

        return true;
    }

    /**
     * 以动画方式打开
     */
    public void openWithAnim() {
        mPreStatus = Status.Moving;
        smoothSlideTo(1);
    }

    /**
     * 以动画方式关闭
     */
    public void closeWithAnim() {
        mPreStatus = Status.Moving;
        smoothSlideTo(0);
    }

    /**
     * 直接打开
     */
    public void open() {
        mPreStatus = Status.Moving;
        slideTo(1);
    }

    /**
     * 直接关闭。如果在AbsListView中删除已经打开的item时，请用该方法关闭item，否则重用item时有问题。RecyclerView中可以用该方法，也可以用closeWithAnim
     */
    public void close() {
        mPreStatus = Status.Moving;
        slideTo(0);
    }

    /**
     * 当前是否为打开状态
     *
     * @return boolean 布尔型
     */
    public boolean isOpened() {
        return (mCurrentStatus == Status.Opened) || (mCurrentStatus == Status.Moving && mPreStatus == Status.Opened);
    }

    /**
     * 当前是否为关闭状态
     *
     * @return boolean 布尔型
     */
    public boolean isClosed() {
        return mCurrentStatus == Status.Closed || (mCurrentStatus == Status.Moving && mPreStatus == Status.Closed);
    }

    /**
     * 获取顶部视图
     *
     * @return Component 组件
     */
    public Component getTopView() {
        return mTopView;
    }

    /**
     * 获取底部视图
     *
     * @return Component 底部视图
     */
    public Component getBottomView() {
        return mBottomView;
    }

    /**
     * 打开或关闭滑动控件
     *
     * @param isOpen 1表示打开，0表示关闭
     */
    private void smoothSlideTo(int isOpen) {
        int mStartLeft = mTopView.getLeft();
        int mFinalLeft = getCloseOrOpenTopViewFinalLeft(isOpen);
        int mOffsetLeft = Math.abs(mStartLeft - mFinalLeft);
        final int durationNum = 300;
        AnimatorValue animatorValue = new AnimatorValue();
        animatorValue.setDuration(durationNum);
        animatorValue.setCurveType(Animator.CurveType.LINEAR);
        animatorValue.setValueUpdateListener((animatorValue1, value) -> {
            float mValue = value;
            if (isOpen == 1) {
                mValue = 1 - value;
            }
            if (mStartLeft > mFinalLeft) {
                mTopLeft = mFinalLeft + (int) (mOffsetLeft * mValue);
            } else {
                mTopLeft = mStartLeft + (int) (mOffsetLeft * mValue);
            }
            postLayout();
        });
        animatorValueStateChanged(animatorValue,isOpen);
        animatorValue.start();
    }

    private void animatorValueStateChanged(AnimatorValue animatorValue,int isOpen) {
        animatorValue.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
                if (isOpen == 1) {
                    mBottomView.setVisibility(VISIBLE);
                    mBottomView.setAlpha(1.0f);
                }
            }
            @Override
            public void onStop(Animator animator) {
            }
            @Override
            public void onCancel(Animator animator) {
            }
            @Override
            public void onEnd(Animator animator) {
                if (isOpen == 1) {
                    mCurrentStatus = Status.Opened;
                    if (mDelegate != null) {
                        mDelegate.onBGASwipeItemLayoutOpened(BGASwipeItemLayout.this);
                    }
                } else {
                    mBottomView.setVisibility(INVISIBLE);
                    mCurrentStatus = Status.Closed;
                    if (mDelegate != null) {
                        mDelegate.onBGASwipeItemLayoutClosed(BGASwipeItemLayout.this);
                    }
                }
                mPreStatus = mCurrentStatus;
            }
            @Override
            public void onPause(Animator animator) {
            }
            @Override
            public void onResume(Animator animator) {
            }
        });
    }

    /**
     * 打开或关闭滑动控件
     *
     * @param isOpen 1表示打开，0表示关闭
     */
    private void slideTo(int isOpen) {
        if (isOpen == 1) {
            mBottomView.setVisibility(VISIBLE);
            mBottomView.setAlpha(1.0f);
            mCurrentStatus = Status.Opened;
            if (mDelegate != null) {
                mDelegate.onBGASwipeItemLayoutOpened(this);
            }
        } else {
            mBottomView.setVisibility(INVISIBLE);
            mCurrentStatus = Status.Closed;
            if (mDelegate != null) {
                mDelegate.onBGASwipeItemLayoutClosed(this);
            }
        }
        mPreStatus = mCurrentStatus;
        mTopLeft = getCloseOrOpenTopViewFinalLeft(isOpen);
        postLayout();
    }

    private int getCloseOrOpenTopViewFinalLeft(int isOpen) {
        int left = /* getPaddingLeft() + */mTopLp.getMarginLeft();
        if (mSwipeDirection == SwipeDirection.Left) {
            left = left - isOpen * mDragRange;
        } else {
            left = left + isOpen * mDragRange;
        }
        return left;
    }

    private void dispatchSwipeEvent() {
        Status preStatus = mCurrentStatus;
        updateCurrentStatus();
        if (mCurrentStatus != preStatus) {
            if (mCurrentStatus == Status.Closed) {
                mBottomView.setVisibility(INVISIBLE);
                if (mDelegate != null && mPreStatus != mCurrentStatus) {
                    mDelegate.onBGASwipeItemLayoutClosed(this);
                }
                mPreStatus = Status.Closed;
            } else if (mCurrentStatus == Status.Opened) {
                if (mDelegate != null && mPreStatus != mCurrentStatus) {
                    mDelegate.onBGASwipeItemLayoutOpened(this);
                }
                mPreStatus = Status.Opened;
            } else if (mPreStatus == Status.Closed) {
                mBottomView.setVisibility(VISIBLE);
                if (mDelegate != null) {
                    mDelegate.onBGASwipeItemLayoutStartOpen(this);
                }
            }
        }
    }

    private void updateCurrentStatus() {
        if (mSwipeDirection == SwipeDirection.Left) {
            if (mTopLeft == /* getPaddingLeft() + */mTopLp.getMarginLeft() - mDragRange) {
                mCurrentStatus = Status.Opened;
            } else if (mTopLeft == /* getPaddingLeft() +*/ mTopLp.getMarginLeft()) {
                mCurrentStatus = Status.Closed;
            } else {
                mCurrentStatus = Status.Moving;
            }
        } else {
            if (mTopLeft == /* getPaddingLeft() + */mTopLp.getMarginLeft() + mDragRange) {
                mCurrentStatus = Status.Opened;
            } else if (mTopLeft == /* getPaddingLeft() + */mTopLp.getMarginLeft()) {
                mCurrentStatus = Status.Closed;
            } else {
                mCurrentStatus = Status.Moving;
            }
        }
    }

    private void cancel() {
        if (mVelocityTracker != null) {
            mVelocityTracker.clear();
            mVelocityTracker = null;
        }
    }

    /**
     * 滑动方向
     *
     * @since 2021-04-22
     */
    public enum SwipeDirection {
        /**
         * 左滑
         */
        Left,
        /**
         * 右滑
         */
        Right
    }

    /**
     * 滑动方式
     *
     * @since 2021-04-22
     */
    public enum BottomModel {
        /**
         * 拉出
         */
        PullOut,
        /**
         * 遮罩
         */
        LayDown
    }

    /**
     * 状态
     *
     * @since 2021-04-22
     */
    public enum Status {
        /**
         * 打开
         */
        Opened,
        /**
         * 关闭
         */
        Closed,
        /**
         * 移动
         */
        Moving
    }

    /**
     * BGASwipeItemLayoutDelegate
     *
     * @since 2021-04-22
     */
    public interface BGASwipeItemLayoutDelegate {
        /**
         * 变为打开状态
         *
         * @param swipeItemLayout swipeItemLayout
         */
        void onBGASwipeItemLayoutOpened(BGASwipeItemLayout swipeItemLayout);

        /**
         * 变为关闭状态
         *
         * @param swipeItemLayout swipeItemLayout
         */
        void onBGASwipeItemLayoutClosed(BGASwipeItemLayout swipeItemLayout);

        /**
         * 从关闭状态切换到正在打开状态
         *
         * @param swipeItemLayout swipeItemLayout
         */
        void onBGASwipeItemLayoutStartOpen(BGASwipeItemLayout swipeItemLayout);
    }

}
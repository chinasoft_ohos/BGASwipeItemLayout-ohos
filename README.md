# BGASwipeItemLayout-ohos


#### 项目介绍
- 项目名称：BGASwipeItemLayout-ohos
- 所属系列：openharmony的第三方组件适配移植
- 功能：实现带弹簧效果的左右滑动控件,可作为ListContainer的item。支持给BGASwipeItemLayout和其子控件设置margin和padding属性
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio 2.2 Beta1
- 基线版本：Release v1.0.4

#### 效果演示
![输入图片说明](https://gitee.com/chinasoft_ohos/BGASwipeItemLayout-ohos/raw/master/img/demo1.gif "demo1.gif")
![输入图片说明](https://gitee.com/chinasoft_ohos/BGASwipeItemLayout-ohos/raw/master/img/demo2.gif "demo2.gif")
![输入图片说明](https://gitee.com/chinasoft_ohos/BGASwipeItemLayout-ohos/raw/master/img/demo3.gif "demo3.gif")
![输入图片说明](https://gitee.com/chinasoft_ohos/BGASwipeItemLayout-ohos/raw/master/img/demo4.gif "demo4.gif")

#### 安装教程

1.在项目根目录下的build.gradle文件中，
 ```gradle
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
 ```

2.在entry模块的build.gradle文件中，
 ```gradle
 dependencies {
	implementation('com.gitee.chinasoft_ohos:BGASwipeItemLayout:1.0.0')
    ......  
 }
```

在sdk6，DevEco Studio 2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

1.BGASwipeItemLayout方法说明

```java
/**
 * 以动画方式打开
 */
public void openWithAnim()

/**
 * 以动画方式关闭
 */
public void closeWithAnim()

/**
 * 直接打开
 */
public void open()

/**
 * 直接关闭
 */
public void close()

/**
 * 当前是否为打开状态
 *
 * @return
 */
public boolean isOpened()

/**
 * 当前是否为关闭状态
 *
 * @return
 */
public boolean isClosed()

/**
 * 获取顶部视图
 *
 * @return
 */
public Component getTopView()

/**
 * 获取底部视图
 *
 * @return
 */
public Component getBottomView()

/**
 * 设置是否可滑动
 *
 * @return
 */
public void setSwipeAble(boolean swipeAble)
```

2.BGASwipeItemLayoutDelegate接口说明

```java
/**
 * 变为打开状态
 *
 * @param swipeItemLayout
 */
void onBGASwipeItemLayoutOpened(BGASwipeItemLayout swipeItemLayout);

/**
 * 变为关闭状态
 *
 * @param swipeItemLayout
 */
void onBGASwipeItemLayoutClosed(BGASwipeItemLayout swipeItemLayout);

/**
 * 从关闭状态切换到正在打开状态
 *
 * @param swipeItemLayout
 */
void onBGASwipeItemLayoutStartOpen(BGASwipeItemLayout swipeItemLayout);
```

3.自定义属性说明
```
	属性名 | 说明 | 默认值
	:----------- | :----------- | :-----------
	bga_sil_swipeDirection     | 往左滑还是往右滑为打开状态(left或right)    | left
	bga_sil_bottomMode         | 底部视图展现方式(layDown或pullOut)         | pullOut
	bga_sil_springDistance     | 弹簧距离          | 0dp
	bga_sil_swipeAble          | 是否可左右滑动    | true
```

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异


#### 版本迭代
- 1.0.0

#### 版权和许可信息
```
	Copyright 2015 bingoogolapple

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
```
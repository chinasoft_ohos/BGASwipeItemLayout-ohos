## 1.0.0
升级正式版

## 0.0.3-SNAPSHOT
* 修改findbugs问题

## 0.0.2-SNAPSHOT
* optimization: Optimize code

## 0.0.1-SNAPSHOT
ohos 第一个版本
 * 实现了原库的大部分 api
 * 因为 "调用ListContainer的notifyDataChanged方法闪退" 原因，"ListView示例"、"RecyclerView示例" 模块的 "ListContainer子元素高度跟随该子元素包含的TextField输入的文字内容高度进行自适应" 功能未能实现

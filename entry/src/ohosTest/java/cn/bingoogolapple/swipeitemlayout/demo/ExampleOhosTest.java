package cn.bingoogolapple.swipeitemlayout.demo;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExampleOhosTest {

    // 纯UI组件不支持单元测试
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("cn.bingoogolapple.swipeitemlayout.demo", actualBundleName);
    }

}
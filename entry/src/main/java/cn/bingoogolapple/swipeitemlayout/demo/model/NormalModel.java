package cn.bingoogolapple.swipeitemlayout.demo.model;

/**
 * NormalModel
 *
 * @since 2021-04-22
 */
public class NormalModel {
    /** 标题
     *
     */
    public String mTitle;

    /** 详细内容
     *
     */
    public String mDetail;

    /** 构造器
     *
     * @param title 标题
     * @param detail 详细内容
     */
    public NormalModel(String title, String detail) {
        mTitle = title;
        mDetail = detail;
    }
}
package cn.bingoogolapple.swipeitemlayout.demo.ability;

import cn.bingoogolapple.swipeitemlayout.BGASwipeItemLayout;
import cn.bingoogolapple.swipeitemlayout.demo.ResourceTable;
import cn.bingoogolapple.swipeitemlayout.demo.Utils.Toast;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.utils.Color;
import ohos.agp.window.service.Window;

/**
 * SwipeItemAbility
 *
 * @since 2021-04-22
 */
public class SwipeItemAbility extends Ability {
    private BGASwipeItemLayout mTestSil;
    private boolean isWithAnimOpen = false;
    private boolean isWithAnimClose = false;

    @Override
    protected void onStart(Intent savedInstanceState) {
        super.onStart(savedInstanceState);
        Window window = getWindow();
        window.setStatusBarColor(Color.getIntColor("#F57157"));
        window.setStatusBarVisibility(0);
        window.setNavigationBarColor(Color.getIntColor("#F46344"));
        setUIContent(ResourceTable.Layout_ability_swipeitem);
        mTestSil = (BGASwipeItemLayout) findComponentById(ResourceTable.Id_sil_swipeitem_test);

        if(null != findComponentById(ResourceTable.Id_iv_swipeitem_avator)){
            findComponentById(ResourceTable.Id_iv_swipeitem_avator).setLongClickedListener(
                    component -> Toast.show(getContext(),"长按了头像"));
            findComponentById(ResourceTable.Id_iv_swipeitem_avator).setClickedListener(
                    component -> onClick(component));
        }

        if(null != findComponentById(ResourceTable.Id_iv_swipeitem_delete)){
            findComponentById(ResourceTable.Id_iv_swipeitem_delete).setLongClickedListener(
                    component -> Toast.show(getContext(),"长按了删除"));
            findComponentById(ResourceTable.Id_iv_swipeitem_delete).setClickedListener(
                    component -> onClick(component));
        }

        if(null != mTestSil){
            mTestSil.setDelegate(new BGASwipeItemLayout.BGASwipeItemLayoutDelegate() {
                @Override
                public void onBGASwipeItemLayoutOpened(BGASwipeItemLayout swipeItemLayout) {
                    if (isWithAnimOpen) {
                        isWithAnimOpen = false;
                        return;
                    }
                    Toast.show(getContext(),"打开");
                }

                @Override
                public void onBGASwipeItemLayoutClosed(BGASwipeItemLayout swipeItemLayout) {
                    if (isWithAnimClose) {
                        isWithAnimClose = false;
                        return;
                    }
                    Toast.show(getContext(),"关闭");
                }

                @Override
                public void onBGASwipeItemLayoutStartOpen(BGASwipeItemLayout swipeItemLayout) {
                    Toast.show(getContext(),"开始打开");
                }

            });
        }

        if(null != findComponentById(ResourceTable.Id_btn_swipeitem_open)){
            findComponentById(ResourceTable.Id_btn_swipeitem_open).setClickedListener(component -> onClick(component));
        }
        if(null != findComponentById(ResourceTable.Id_btn_swipeitem_close)){
            findComponentById(ResourceTable.Id_btn_swipeitem_close).setClickedListener(component -> onClick(component));
        }
        if(null != findComponentById(ResourceTable.Id_btn_swipeitem_openwithanim)){
            findComponentById(ResourceTable.Id_btn_swipeitem_openwithanim).setClickedListener(
                    component -> onClick(component));
        }
        if(null != findComponentById(ResourceTable.Id_btn_swipeitem_closewithanim)){
            findComponentById(ResourceTable.Id_btn_swipeitem_closewithanim).setClickedListener(
                    component -> onClick(component));
        }
        if(null != findComponentById(ResourceTable.Id_btn_swipeitem_status)){
            findComponentById(ResourceTable.Id_btn_swipeitem_status).setClickedListener(component -> onClick(component));
        }
    }


    /** 点击事件
     *
     * @param view 组件
     */
    public void onClick(Component view) {
        switch (view.getId()) {
            case ResourceTable.Id_iv_swipeitem_avator:
                Toast.show(getContext(),"点击了头像");
                break;
            case ResourceTable.Id_iv_swipeitem_delete:
                Toast.show(getContext(),"点击了删除");
                break;
            case ResourceTable.Id_btn_swipeitem_open:
                if (null != mTestSil) {
                    mTestSil.open();
                }
                break;
            case ResourceTable.Id_btn_swipeitem_close:
                if (null != mTestSil) {
                    mTestSil.close();
                }
                break;
            case ResourceTable.Id_btn_swipeitem_openwithanim:
                if (null != mTestSil) {
                    if (mTestSil.getmPreStatus() == BGASwipeItemLayout.Status.Opened) {
                        isWithAnimOpen = true;
                        return;
                    }
                    mTestSil.openWithAnim();
                }
                break;
            case ResourceTable.Id_btn_swipeitem_closewithanim:
                if (null != mTestSil) {
                    if (mTestSil.getmPreStatus() == BGASwipeItemLayout.Status.Closed) {
                        isWithAnimClose = true;
                        return;
                    }
                    mTestSil.closeWithAnim();
                }
                break;
            case ResourceTable.Id_btn_swipeitem_status:
                showStatus();
                break;
            default:
                break;
        }
    }

    /** 当前滑块状态
     *
     */
    private void showStatus() {
        if (null != mTestSil) {
            if (mTestSil.isOpened()) {
                Toast.show(getContext(),"打开状态");
            } else if (mTestSil.isClosed()) {
                Toast.show(getContext(),"关闭状态");
            }
        }
    }
}
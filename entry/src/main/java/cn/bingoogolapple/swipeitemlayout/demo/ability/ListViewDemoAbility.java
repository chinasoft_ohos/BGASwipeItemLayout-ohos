package cn.bingoogolapple.swipeitemlayout.demo.ability;

import cn.bingoogolapple.swipeitemlayout.demo.ResourceTable;
import cn.bingoogolapple.swipeitemlayout.demo.interfaces.BgaOnItemChildLongClickListener;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.ListContainer;

import java.util.List;

import cn.bingoogolapple.swipeitemlayout.demo.Utils.Toast;
import cn.bingoogolapple.swipeitemlayout.demo.adapter.AdapterViewAdapter;
import cn.bingoogolapple.swipeitemlayout.demo.engine.DataEngine;
import cn.bingoogolapple.swipeitemlayout.demo.interfaces.BgaOnItemChildClickListener;
import cn.bingoogolapple.swipeitemlayout.demo.model.NormalModel;
import ohos.agp.utils.Color;
import ohos.agp.window.service.Window;

/**
 * ListViewDemoAbility
 *
 * @since 2021-04-22
 */
public class ListViewDemoAbility extends Ability implements Component.ClickedListener,
        ComponentContainer.LongClickedListener, BgaOnItemChildClickListener, BgaOnItemChildLongClickListener {
    private List<NormalModel> mData;
    private ListContainer mDataLv;
    private AdapterViewAdapter mAdapter;
    @Override
    protected void onStart(Intent savedInstanceState) {
        super.onStart(savedInstanceState);
        Window window = getWindow();
        window.setStatusBarColor(Color.getIntColor("#F57157"));
        window.setStatusBarVisibility(0);
        window.setNavigationBarColor(Color.getIntColor("#F46344"));
        setUIContent(ResourceTable.Layout_ability_listview);
        initListView();
    }

    /** 初始化列表
     *
     */
    private void initListView() {
        // 获取listcontainer布局文件
        mDataLv = (ListContainer) findComponentById(ResourceTable.Id_lv_listview_data);

        // 给listcontainer每个子元素设置点击跟长按监听
        mDataLv.setItemClickedListener(new ListContainer.ItemClickedListener() {
            @Override
            public void onItemClicked(ListContainer listContainer, Component component, int i, long l) {
                Toast.show(getContext(),"点击了条目 " + (mAdapter.getItem(i).mTitle));
            }
        });
        mDataLv.setItemLongClickedListener(new ListContainer.ItemLongClickedListener() {
            @Override
            public boolean onItemLongClicked(ListContainer listContainer, Component component, int i, long l) {
                Toast.show(getContext(),"长按了" + (mAdapter.getItem(i).mTitle));
                return true;
            }
        });

        // 创建adapter
        mAdapter = new AdapterViewAdapter(this);

        // 获取数据以及给adapter设置子元素数据
        mData = DataEngine.loadNormalModelDatas();
        mAdapter.setData(mData);
        mAdapter.setOnItemChildClickListener(this);
        mAdapter.setOnItemChildLongClickListener(this);

        // 给listcontainer设置adapter
        mDataLv.setItemProvider(mAdapter);

        // 给listcontainer设置滚动监听
        mDataLv.setScrolledListener(new Component.ScrolledListener() {
            @Override
            public void onContentScrolled(Component component, int i, int i1, int i2, int i3) {

            }
            @Override
            public void scrolledStageUpdate(Component component, int newStage) {
                mAdapter.closeOpenedSwipeItemLayoutWithAnim();
            }
        });
    }

    @Override
    public void onClick(Component component) {

    }

    @Override
    public void onLongClicked(Component component) {

    }

    @Override
    public void onItemChildClick(ComponentContainer parent, Component childView, int position) {
        if (childView.getId() == ResourceTable.Id_tv_item_bgaswipe_delete) {
            // 作为ListView的item使用时，如果删除了某一个item，请先关闭已经打开的item，否则其他item会显示不正常（RecyclerView不会有这个问题）

            mAdapter.closeOpenedSwipeItemLayout();
            mAdapter.removeItem(position);
        }
    }

    @Override
    public boolean onItemChildLongClick(ComponentContainer parent, Component childView, int position) {
        if (childView.getId() == ResourceTable.Id_tv_item_bgaswipe_delete) {
            Toast.show(getContext(),"长按了删除 " + (mAdapter.getItem(position).mTitle));
            return true;
        }
        return false;
    }
}
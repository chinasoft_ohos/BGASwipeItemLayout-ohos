package cn.bingoogolapple.swipeitemlayout.demo.adapter;

import java.util.ArrayList;
import java.util.List;

import cn.bingoogolapple.swipeitemlayout.BGASwipeItemLayout;
import cn.bingoogolapple.swipeitemlayout.demo.ResourceTable;
import cn.bingoogolapple.swipeitemlayout.demo.Utils.Util;
import cn.bingoogolapple.swipeitemlayout.demo.ability.RecyclerViewDemoAbility;
import cn.bingoogolapple.swipeitemlayout.demo.model.NormalModel;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;
import ohos.agp.components.Text;


/**
 * RecyclerViewAdapter
 *
 * @since 2021-04-22
 */
public class RecyclerViewAdapter extends BaseItemProvider {
    private static final int DIVISOR_NUM = 3;
    /**
     * 当前处于打开状态的item
     */
    private List<BGASwipeItemLayout> mOpenedSil = new ArrayList<>();
    private ListContainer mRecyclerView;
    private List<NormalModel> mData;
    private RecyclerViewDemoAbility onRvItemClickListener;
    private RecyclerViewDemoAbility onRvItemLongClickListener;
    private RecyclerViewDemoAbility onItemChildClickListener;
    private RecyclerViewDemoAbility onItemChildLongClickListener;

    /** 构造器
     *
     * @param recyclerView 列表组件
     */
    public RecyclerViewAdapter(ListContainer recyclerView) {
        super();
        mRecyclerView = recyclerView;
    }

    /** 设置数据
     *
     * @param data 列表数据
     */
    public void setData(List<NormalModel> data) {
        if (Util.isListNotEmpty(data)) {
            this.mData = data;
        } else {
            this.mData.clear();
        }
        notifyDataChanged();
    }

    /** 设置子元素监听
     *
     * @param componentContainer 根组件
     * @param viewHolderHelper 父组件
     * @param position 子组件下标
     * @param viewType 子组件类型
     */
    public void setItemChildListener(ComponentContainer componentContainer,
                                     Component viewHolderHelper, int position, int viewType) {
        BGASwipeItemLayout swipeItemLayout = (BGASwipeItemLayout) viewHolderHelper.
                findComponentById(ResourceTable.Id_sil_item_bgaswipe_root);
        swipeItemLayout.setDelegate(new BGASwipeItemLayout.BGASwipeItemLayoutDelegate() {
            @Override
            public void onBGASwipeItemLayoutOpened(BGASwipeItemLayout swipeItemLayout) {
                closeOpenedSwipeItemLayoutWithAnim();
                mOpenedSil.add(swipeItemLayout);
            }

            @Override
            public void onBGASwipeItemLayoutClosed(BGASwipeItemLayout swipeItemLayout) {
                mOpenedSil.remove(swipeItemLayout);
            }

            @Override
            public void onBGASwipeItemLayoutStartOpen(BGASwipeItemLayout swipeItemLayout) {
                closeOpenedSwipeItemLayoutWithAnim();
            }
        });
        swipeItemLayout.setClickedListener(component -> {
            if (onRvItemClickListener != null) {
                onRvItemClickListener.onRvItemClick(componentContainer, component, position);
            }
        });
        swipeItemLayout.setLongClickedListener(component -> {
            if (onRvItemLongClickListener != null) {
                onRvItemLongClickListener.onRvItemLongClick(componentContainer, component, position);
            }
        });
        Text delete = (Text) swipeItemLayout.findComponentById(ResourceTable.Id_tv_item_bgaswipe_delete);

        // 给删除按钮添加点击监听
        delete.setClickedListener(component -> {
            if (onItemChildClickListener != null) {
                onItemChildClickListener.onItemChildClick(componentContainer, component, position);
            }
        });

        // 给删除按钮添加长按监听
        delete.setLongClickedListener(new Component.LongClickedListener() {
            @Override
            public void onLongClicked(Component component) {
                if (onItemChildLongClickListener != null) {
                    onItemChildLongClickListener.onItemChildLongClick(componentContainer, component, position);
                }
            }
        });
    }

    /** 填充每个Component下面BGASwipeItemLayout下面的组件的值以及BGASwipeItemLayout是否能滑动
     *
     * @param viewHolderHelper 父组件
     * @param position 子组件下标
     * @param model 填充数据
     */
    public void fillData(Component viewHolderHelper, int position, NormalModel model) {
        BGASwipeItemLayout swipeItemLayout = (BGASwipeItemLayout) viewHolderHelper.
                findComponentById(ResourceTable.Id_sil_item_bgaswipe_root);
        if (position % DIVISOR_NUM == 0) {
            swipeItemLayout.setSwipeAble(false);
        } else {
            swipeItemLayout.setSwipeAble(true);
        }

        Text text1 = (Text) swipeItemLayout.findComponentById(ResourceTable.Id_tv_item_bgaswipe_title);
        text1.setText(model.mTitle);
        Text text2 = (Text) swipeItemLayout.findComponentById(ResourceTable.Id_tv_item_bgaswipe_detail);
        text2.setText(model.mDetail);
        Text text3 = (Text) swipeItemLayout.findComponentById(ResourceTable.Id_et_item_bgaswipe_title);
        text3.setText(model.mTitle);
    }

    /** 动画效果关闭滑块
     *
     */
    public void closeOpenedSwipeItemLayoutWithAnim() {
        for (BGASwipeItemLayout sil : mOpenedSil) {
            sil.closeWithAnim();
        }
        mOpenedSil.clear();
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public NormalModel getItem(int i) {
        return mData.get(i);
    }

    @Override
    public long getItemId(int itemId) {
        return itemId;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        Component view;
        if (component == null) {
            view = LayoutScatter.getInstance(mRecyclerView.getContext()).
                    parse(ResourceTable.Layout_item_bgaswipe,componentContainer,false);
        } else {
            view = component;
        }
        setItemChildListener(componentContainer, view, i,0);
        fillData(view,i,mData.get(i));
        return view;
    }

    /** 删除列表子数据
     *
     * @param position 下标
     */
    public void removeItem(int position) {
        mData.remove(position);
        notifyDataChanged();
    }

    public void setOnRvItemClickListener(RecyclerViewDemoAbility onRvItemClickListener) {
        this.onRvItemClickListener = onRvItemClickListener;
    }

    public void setOnRvItemLongClickListener(RecyclerViewDemoAbility onRvItemLongClickListener) {
        this.onRvItemLongClickListener = onRvItemLongClickListener;
    }

    public void setOnItemChildClickListener(RecyclerViewDemoAbility onItemChildClickListener) {
        this.onItemChildClickListener = onItemChildClickListener;
    }

    public void setOnItemChildLongClickListener(RecyclerViewDemoAbility onItemChildLongClickListener) {
        this.onItemChildLongClickListener = onItemChildLongClickListener;
    }
}
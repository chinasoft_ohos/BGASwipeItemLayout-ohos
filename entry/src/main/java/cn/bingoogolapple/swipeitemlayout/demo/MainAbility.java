package cn.bingoogolapple.swipeitemlayout.demo;

import cn.bingoogolapple.swipeitemlayout.demo.slice.MainAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.utils.Color;
import ohos.agp.window.service.Window;

/**
 * MainAbility
 *
 * @since 2021-04-22
 */
public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        Window window = getWindow();
        window.setStatusBarColor(Color.getIntColor("#F57157"));
        window.setStatusBarVisibility(0);
        window.setNavigationBarColor(Color.getIntColor("#F46344"));
        super.setMainRoute(MainAbilitySlice.class.getName());
    }
}

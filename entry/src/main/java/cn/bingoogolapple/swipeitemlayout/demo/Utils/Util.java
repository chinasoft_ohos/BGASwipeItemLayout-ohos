/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.bingoogolapple.swipeitemlayout.demo.Utils;

import java.util.List;

/**
 * Util
 *
 * @since 2021-04-22
 */
public final class Util {

    /**
     * 判断list是否为空
     *
     * @param list list集合
     * @return boolean 布尔型
     */
    public static boolean isListNotEmpty(List list) {
        return list != null && !list.isEmpty();
    }
}

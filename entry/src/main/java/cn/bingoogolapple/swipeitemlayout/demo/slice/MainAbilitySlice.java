package cn.bingoogolapple.swipeitemlayout.demo.slice;

import cn.bingoogolapple.swipeitemlayout.demo.ResourceTable;
import cn.bingoogolapple.swipeitemlayout.demo.ability.ListViewDemoAbility;
import cn.bingoogolapple.swipeitemlayout.demo.ability.RecyclerViewDemoAbility;
import cn.bingoogolapple.swipeitemlayout.demo.ability.SwipeItemAbility;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Button;
import ohos.agp.components.Component;

/**
 * MainAbilitySlice
 *
 * @since 2021-04-22
 */
public class MainAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        Button button1 = (Button) findComponentById(ResourceTable.Id_button_1);
        button1.setClickedListener(component -> changeToSwipeItemDemo(component));
        Button button2 = (Button) findComponentById(ResourceTable.Id_button_2);
        button2.setClickedListener(component -> changeToListViewDemo(component));
        Button button3 = (Button) findComponentById(ResourceTable.Id_button_3);
        button3.setClickedListener(component -> changeToRecyclerViewDemo(component));
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    /** 跳转到第一个页面
     *
     * @param component 组件
     */
    public void changeToSwipeItemDemo(Component component) {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(getBundleName())
                .withAbilityName(SwipeItemAbility.class.getName())
                .build();
        intent.setOperation(operation);
        startAbility(intent);
    }

    /** 跳转到第二个页面
     *
     * @param component 组件
     */
    public void changeToListViewDemo(Component component) {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(getBundleName())
                .withAbilityName(ListViewDemoAbility.class.getName())
                .build();
        intent.setOperation(operation);
        startAbility(intent);
    }

    /** 跳转到第三个页面
     *
     * @param component 组件
     */
    public void changeToRecyclerViewDemo(Component component) {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(getBundleName())
                .withAbilityName(RecyclerViewDemoAbility.class.getName())
                .build();
        intent.setOperation(operation);
        startAbility(intent);
    }
}

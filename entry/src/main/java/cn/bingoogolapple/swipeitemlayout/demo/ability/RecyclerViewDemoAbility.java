package cn.bingoogolapple.swipeitemlayout.demo.ability;

import java.util.List;
import cn.bingoogolapple.swipeitemlayout.demo.ResourceTable;
import cn.bingoogolapple.swipeitemlayout.demo.Utils.Toast;
import cn.bingoogolapple.swipeitemlayout.demo.adapter.RecyclerViewAdapter;
import cn.bingoogolapple.swipeitemlayout.demo.engine.DataEngine;
import cn.bingoogolapple.swipeitemlayout.demo.interfaces.BgaOnItemChildClickListener;
import cn.bingoogolapple.swipeitemlayout.demo.interfaces.BgaOnItemChildLongClickListener;
import cn.bingoogolapple.swipeitemlayout.demo.interfaces.BgaOnRvItemClickListener;
import cn.bingoogolapple.swipeitemlayout.demo.interfaces.BgaOnRvItemLongClickListener;
import cn.bingoogolapple.swipeitemlayout.demo.model.NormalModel;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayoutManager;
import ohos.agp.components.ListContainer;
import ohos.agp.utils.Color;
import ohos.agp.window.service.Window;

/**
 * RecyclerViewDemoAbility
 *
 * @since 2021-04-22
 */
public class RecyclerViewDemoAbility extends Ability implements BgaOnRvItemClickListener,
        BgaOnRvItemLongClickListener, BgaOnItemChildClickListener, BgaOnItemChildLongClickListener {
    private RecyclerViewAdapter mAdapter;
    private List<NormalModel> mData;
    private ListContainer mDataRv;
    @Override
    protected void onStart(Intent savedInstanceState) {
        super.onStart(savedInstanceState);
        Window window = getWindow();
        window.setStatusBarColor(Color.getIntColor("#F57157"));
        window.setStatusBarVisibility(0);
        window.setNavigationBarColor(Color.getIntColor("#F46344"));
        setUIContent(ResourceTable.Layout_ability_recyclerview);
        initRecyclerView();
    }

    /** 初始化列表
     *
     */
    private void initRecyclerView() {
        mDataRv = (ListContainer) findComponentById(ResourceTable.Id_rv_recyclerview_data);
        DirectionalLayoutManager layoutManager = new DirectionalLayoutManager();
        layoutManager.setOrientation(Component.VERTICAL);
        mDataRv.setLayoutManager(layoutManager);

        mAdapter = new RecyclerViewAdapter(mDataRv);
        mAdapter.setOnRvItemClickListener(this);
        mAdapter.setOnRvItemLongClickListener(this);
        mAdapter.setOnItemChildClickListener(this);
        mAdapter.setOnItemChildLongClickListener(this);

        mDataRv.setScrolledListener(new ListContainer.ScrolledListener() {
            @Override
            public void onContentScrolled(Component component, int i, int i1, int i2, int i3) {

            }
            @Override
            public void scrolledStageUpdate(Component component, int newStage) {
                mAdapter.closeOpenedSwipeItemLayoutWithAnim();
            }
        });

        mData = DataEngine.loadNormalModelDatas();
        mAdapter.setData(mData);
        mDataRv.setItemProvider(mAdapter);
    }

    @Override
    public void onItemChildClick(ComponentContainer parent, Component childView, int position) {
        if (childView.getId() == ResourceTable.Id_tv_item_bgaswipe_delete) {
            mAdapter.closeOpenedSwipeItemLayoutWithAnim();
            mAdapter.removeItem(position);
        }
    }

    @Override
    public boolean onItemChildLongClick(ComponentContainer parent, Component childView, int position) {
        if (childView.getId() == ResourceTable.Id_tv_item_bgaswipe_delete) {
            Toast.show(getContext(),"长按了删除 " + mAdapter.getItem(position).mTitle);
            return true;
        }
        return false;
    }

    @Override
    public void onRvItemClick(ComponentContainer parent, Component itemView, int position) {
        Toast.show(getContext(),"点击了条目 " + mAdapter.getItem(position).mTitle);
    }

    @Override
    public boolean onRvItemLongClick(ComponentContainer parent, Component itemView, int position) {
        Toast.show(getContext(),"长按了条目 " + mAdapter.getItem(position).mTitle);
        return true;
    }
}
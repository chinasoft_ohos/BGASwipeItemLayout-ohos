/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.bingoogolapple.swipeitemlayout.demo.engine;

import java.util.ArrayList;
import java.util.List;

import cn.bingoogolapple.swipeitemlayout.demo.model.NormalModel;

/**
 * DataEngine
 *
 * @since 2021-04-22
 */
public class DataEngine {
    private static final int LOOP_NUM = 20;
    private static final int DIVISOR_NUM = 4;
    /** 构建数据
     *
     * @return  List<~> List集合
     */
    public static List<NormalModel> loadNormalModelDatas() {
        List<NormalModel> datas = new ArrayList<>();
        for (int index = 0; index < LOOP_NUM; index++) {
            if (index % DIVISOR_NUM == 0) {
                datas.add(new NormalModel("标题" + index, "我是短的描述" + index));
            } else {
                datas.add(new NormalModel("标题" + index,
                        "我是很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长的描述" + index));
            }
        }
        return datas;
    }
}
package cn.bingoogolapple.swipeitemlayout.demo.adapter;

import java.util.ArrayList;
import java.util.List;

import cn.bingoogolapple.swipeitemlayout.BGASwipeItemLayout;
import cn.bingoogolapple.swipeitemlayout.demo.ResourceTable;
import cn.bingoogolapple.swipeitemlayout.demo.Utils.Util;
import cn.bingoogolapple.swipeitemlayout.demo.ability.ListViewDemoAbility;
import cn.bingoogolapple.swipeitemlayout.demo.model.NormalModel;
import ohos.accessibility.ability.SoftKeyBoardController;
import ohos.accessibility.ability.SoftKeyBoardListener;
import ohos.agp.components.*;
import ohos.app.Context;

/**
 * AdapterViewAdapter
 *
 * @since 2021-04-22
 */
public class AdapterViewAdapter extends BaseItemProvider implements SoftKeyBoardListener {
    private static final int DIVISOR_NUM = 3;
    /**
     * 当前处于打开状态的item
     */
    private List<BGASwipeItemLayout> mOpenedSil = new ArrayList<>();
    private ListViewDemoAbility onItemChildClickListener;
    private ListViewDemoAbility onItemChildLongClickListener;
    private List<NormalModel> mData;
    private Context mContext;

    /** 构造器
     *
     * @param context 上下文
     */
    public AdapterViewAdapter(Context context) {
        super();
        mContext = context;
    }

    /** 设置数据
     *
     * @param data 列表数据
     */
    public void setData(List<NormalModel> data) {
        if (Util.isListNotEmpty(data)) {
            this.mData = data;
        } else {
            this.mData.clear();
        }
        notifyDataChanged();
    }

    /** 设置子元素监听
     *
     * @param componentContainer 根组件
     * @param viewHolderHelper 父组件
     * @param position 子组件下标
     */
    protected void setItemChildListener(ComponentContainer componentContainer,
                                        Component viewHolderHelper, int position) {
        // 列表子BGASwipeItemLayout
        BGASwipeItemLayout swipeItemLayout = (BGASwipeItemLayout)
                viewHolderHelper.findComponentById(ResourceTable.Id_sil_item_bgaswipe_root);

        swipeItemLayout.setDelegate(new BGASwipeItemLayout.BGASwipeItemLayoutDelegate() {
            @Override
            public void onBGASwipeItemLayoutOpened(BGASwipeItemLayout swipeItemLayout) {
                closeOpenedSwipeItemLayoutWithAnim();
                mOpenedSil.add(swipeItemLayout);
            }

            @Override
            public void onBGASwipeItemLayoutClosed(BGASwipeItemLayout swipeItemLayout) {
                mOpenedSil.remove(swipeItemLayout);
            }

            @Override
            public void onBGASwipeItemLayoutStartOpen(BGASwipeItemLayout swipeItemLayout) {
                closeOpenedSwipeItemLayoutWithAnim();
            }
        });
        Text delete = (Text) swipeItemLayout.findComponentById(ResourceTable.Id_tv_item_bgaswipe_delete);
        // 给删除按钮添加点击监听
        delete.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (onItemChildClickListener != null) {
                    onItemChildClickListener.onItemChildClick(componentContainer, component, position);
                }
            }
        });

        // 给删除按钮添加长按监听
        delete.setLongClickedListener(new Component.LongClickedListener() {
            @Override
            public void onLongClicked(Component component) {
                if (onItemChildLongClickListener != null) {
                    onItemChildLongClickListener.onItemChildLongClick(componentContainer, component, position);
                }
            }
        });

    }

    /** 填充每个Component下面BGASwipeItemLayout下面的组件的值以及BGASwipeItemLayout是否能滑动
     *
     * @param viewHolderHelper 父组件
     * @param position 子组件下标
     * @param model 填充数据
     */
    public void fillData(Component viewHolderHelper, int position, NormalModel model) {
        BGASwipeItemLayout swipeItemLayout = (BGASwipeItemLayout) viewHolderHelper.
                findComponentById(ResourceTable.Id_sil_item_bgaswipe_root);
        Text text1 = (Text) swipeItemLayout.findComponentById(ResourceTable.Id_tv_item_bgaswipe_title);
        text1.setText(model.mTitle);
        Text text2 = (Text) swipeItemLayout.findComponentById(ResourceTable.Id_tv_item_bgaswipe_detail);
        text2.setText(model.mDetail);
        TextField text3 = (TextField) swipeItemLayout.findComponentById(ResourceTable.Id_et_item_bgaswipe_title);
        text3.setText(model.mTitle);
        text3.addTextObserver(new Text.TextObserver() {
            @Override
            public void onTextUpdated(String s, int i, int i1, int i2) { }
        });
        if (position % DIVISOR_NUM == 0) {
            swipeItemLayout.setSwipeAble(false);
        } else {
            swipeItemLayout.setSwipeAble(true);
        }
    }

    /** 动画效果关闭滑块
     *
     */
    public void closeOpenedSwipeItemLayoutWithAnim() {
        for (BGASwipeItemLayout sil : mOpenedSil) {
            sil.closeWithAnim();
        }
        mOpenedSil.clear();
    }

    /** 关闭打开的滑块
     *
     */
    public void closeOpenedSwipeItemLayout() {
        for (BGASwipeItemLayout sil : mOpenedSil) {
            sil.close();
        }
        mOpenedSil.clear();
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public NormalModel getItem(int i) {
        return mData.get(i);
    }

    @Override
    public long getItemId(int itemId) {
        return itemId;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        Component view;
        if (component == null) {
            view = LayoutScatter.getInstance(mContext).
                    parse(ResourceTable.Layout_item_bgaswipe,componentContainer,false);
        } else {
            view = component;
        }
        setItemChildListener(componentContainer, view, i);
        fillData(view,i,mData.get(i));
        return view;
    }

    /** 关闭打开的滑块
     * @param position 子元素下标
     */
    public void removeItem(int position) {
        mData.remove(position);
        notifyDataChanged();
    }

    public void setOnItemChildClickListener(ListViewDemoAbility onItemChildClickListener) {
        this.onItemChildClickListener = onItemChildClickListener;
    }

    public void setOnItemChildLongClickListener(ListViewDemoAbility onItemChildLongClickListener) {
        this.onItemChildLongClickListener = onItemChildLongClickListener;
    }

    @Override
    public void onSoftKeyBoardShowModeChanged(SoftKeyBoardController softKeyBoardController, int i) {

    }
}